package quind.training.calculator.employee.domain;

public class Employee {
	
	private String name, idNumber;
	private int id;
	private double initialSalary, finalSalary;

	public Employee (int id, String name, String idNumber, double initialSalary, double finalSalary) {
		this.setId(id);
		this.name = name;
		this.idNumber = idNumber;
		this.initialSalary = initialSalary;
		this.finalSalary=finalSalary;
	}
	
	public Employee () {
		super();
	}
	
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idnumber) {
		this.idNumber = idnumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getInitialSalary() {
		return initialSalary;
	}
	public void setInitialSalary(double initialSalary) {
		this.initialSalary = initialSalary;
	}
	public double getFinalSalary() {
		return finalSalary;
	}
	public void setFinalSalary(double finalSalary) {
		this.finalSalary = finalSalary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
