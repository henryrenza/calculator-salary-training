package quind.training.calculator.employee.domain;

import javax.swing.plaf.synth.SynthOptionPaneUI;

public class SalaryCalculator extends Calculator{
	
	private double initialSalary;
	private double foreingTax;
	private double socialServiceTax;
	private double profesionalBonus;
	private String employeeType; //local, professional, foreing
	
	private static final double  FOREING_TAX = 0.15;
	private static final double  SOCIAL_SERVICE_TAX= 0.25;
	private static final double  PROFESIONAL_BONUS = 0.20;
	
	public SalaryCalculator () {
		super ();
		this.setEmployeeType(null);
		this.setInitialSalary(0);
	}
	
	public double CalculateFinalSalaryDependingTypeEmployee (double initialSalary, String employeeType) {
		this.setInitialSalary(initialSalary);
		this.setEmployeeType(employeeType.toLowerCase());
		foreingTax = this.initialSalary*FOREING_TAX;
		socialServiceTax = this.initialSalary*SOCIAL_SERVICE_TAX;
		profesionalBonus = this.initialSalary*PROFESIONAL_BONUS;
		
		switch (this.employeeType) {
		
		case ("foreing"):
			return this.initialSalary - foreingTax - socialServiceTax;
		
		case ("professional"):
			return this.initialSalary + profesionalBonus - socialServiceTax;
		
		case "local":
			return this.initialSalary - socialServiceTax;
			
		default:
			return 0.0;
		}	
	}
			
	public void setInitialSalary(double initialSalary) {
		this.initialSalary = initialSalary;
	}
	
	public void setForeingTax(double foreingTax) {
		this.foreingTax = foreingTax;
	}
	
	public void setSocialServiceTax(double socialServiceTax) {
		this.socialServiceTax = socialServiceTax;
	}
	
	public void setProfesionalBonus(double profesionalBonus) {
		this.profesionalBonus = profesionalBonus;
	}
	
	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	public double getInitialSalary() {
		return initialSalary;
	}
	
	public double getForeingTax() {
		return foreingTax;
	}

	public double getSocialServiceTax() {
		return socialServiceTax;
	}

	public double getProfesionalBonus() {
		return profesionalBonus;
	}
	
	public String getEmployeeType() {
		return employeeType;
	}
	
	
	
}
