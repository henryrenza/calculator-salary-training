package quind.training.calculator.employee.domain;

public class Calculator {

	private final double num1, num2;
	private double result;

	public Calculator(double num1, double num2) {
		this.num1 = num1;
		this.num2 = num2;
	}
	
	public Calculator() {
		this.num1 = 0;
		this.num2 = 0;
	}

	public double getNum1() {
		return num1;
	}

	public double getNum2() {
		return num2;
	}

	public double sumOfTwoNumbers(double num1, double num2) {
		result = num1 + num2;
		return result;
	}

	public double subtractOfTwoNumbers(double num1, double num2) {
		result = num1 - num2;
		return result;
	}

	public double MultiplicationOfTwoNumbers(double num1, double num2) {
		result = num1 * num2;
		return result;
	}

	public double divisionOfTwoNumbers(double num1, double num2) {
		if (num2==0) {System.out.println("ERROR: This operation cannot be realized");}
		result = num1 / num2;
		return result;
	}

}