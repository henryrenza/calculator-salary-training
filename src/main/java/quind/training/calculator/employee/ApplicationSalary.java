package quind.training.calculator.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationSalary {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationSalary.class, args);
	}
}
