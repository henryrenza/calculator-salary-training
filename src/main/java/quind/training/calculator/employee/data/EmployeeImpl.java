package quind.training.calculator.employee.data;

import java.sql.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import quind.training.calculator.employee.domain.Employee;

@Repository
public class EmployeeImpl implements IEmployeeDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
    public String save(final Employee employee) {
        String query = "INSERT INTO Employee(name, idNumber, initialSalary, finalSalary) VALUES(?,?,?,?)";
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                //ps.setInt(1, employee.getId());
                ps.setString(1, employee.getName());
                ps.setString(2, employee.getIdNumber());
                ps.setDouble(3, employee.getInitialSalary());
                ps.setDouble(4, employee.getFinalSalary());
                return ps;
            }
        }, holder);
        return "ADDED";
    }


	@Override
	public String update(Employee employee) {
		String query = "UPDATE Employee SET name=?, idNumber=?, initialSalary=?, finalSalary=? WHERE id = ?";
		return "MODIFIED"+jdbcTemplate.update(query, employee.getName(), employee.getIdNumber(), employee.getInitialSalary(),employee.getFinalSalary(), employee.getId());    
	}
	
	@Override
	public Employee findById(int id) {
		 String query = "SELECT * FROM Employee WHERE id = ?";
		 return jdbcTemplate.queryForObject(query, new Object[] { id }, new employeeMapper());
	}
	
	@Override
	public List<Employee> findAll() {
		String query = "SELECT * FROM Employee";
		return jdbcTemplate.query(query, new employeeMapper());
	}

	@Override
	public String delete(int id) {
		String query = "DELETE FROM Employee WHERE ID = ?";
        return "DELETED "+jdbcTemplate.update(query, id);
	}
	
	class employeeMapper implements RowMapper<Employee> {
        @Override
        public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Employee employee = new Employee();
        	employee.setId(rs.getInt("id"));
        	employee.setName(rs.getString("name"));	
        	employee.setIdNumber(rs.getString("idNumber"));
        	employee.setInitialSalary(rs.getDouble("initialSalary"));
        	employee.setFinalSalary(rs.getDouble("finalSalary"));
            return employee;
        }
        
     }
}


