package quind.training.calculator.employee.data;

import java.util.List;

import quind.training.calculator.employee.domain.Employee;

public interface IEmployeeDao {
	
		public String save(Employee employee);
		
	    public String update(Employee employee);
	    
	    public Employee findById(int id);
	    
	    public List<Employee> findAll();
	    
	    public String delete(int id);
	    
	}

