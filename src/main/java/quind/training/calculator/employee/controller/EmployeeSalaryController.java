package quind.training.calculator.employee.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import quind.training.calculator.employee.domain.Calculator;
import quind.training.calculator.employee.domain.SalaryCalculator;

@RestController
public class EmployeeSalaryController {

	Calculator calculator = new Calculator(0, 0);
	SalaryCalculator salarycalculator = new SalaryCalculator();
	
	@GetMapping("/gretting")
	public String gretting() {
		return ("Hello & Wellcome to Salary Calculator API");
	}

	@GetMapping("/sum")
	public String sumaOfTwoNumbers(@RequestParam(value = "num1") String num1,
			@RequestParam(value = "num2") String num2) {
		return "FIRST VALUE:" + num1 + "   SECOND VALUE:" + num2 + "   SUM OF BOTH VALUES:"
				+ calculator.sumOfTwoNumbers(Double.parseDouble(num1), Double.parseDouble(num2));
	}

	@GetMapping("/subtract")
	public String subtractOfTwoNumbers(@RequestParam(value = "num1") String num1,
			@RequestParam(value = "num2") String num2) {
		return "FIRST VALUE:" + num1 + "   SECOND VALUE:" + num2 + "   SUBTRACT OF BOTH VALUES:"
				+ calculator.subtractOfTwoNumbers(Double.parseDouble(num1), Double.parseDouble(num2));
	}

	@GetMapping("/multiplication")
	public String multiplicactionOfTwoNumbers(@RequestParam(value = "num1") String num1,
			@RequestParam(value = "num2") String num2) {
		
		return "FIRST VALUE:" + num1 + "   SECOND VALUE:" + num2 + "   MULTIPLICATION OF BOTH VALUES:"
				+ calculator.MultiplicationOfTwoNumbers(Double.parseDouble(num1), Double.parseDouble(num2));
	}

	@GetMapping("/division")
	public String divisionOfTwoNumbers(@RequestParam(value = "num1") String num1,
			@RequestParam(value = "num2") String num2) {
		if (num2.equals("0")) {
			return "FIRST VALUE:" + num1 + "   SECOND VALUE:" + num2 + "   DIVISION OF BOTH VALUE IS NOT POSSIBLE";
		}
		else	{	
		return "FIRST VALUE:" + num1 + "   SECOND VALUE:" + num2 + "   DIVISION OF BOTH VALUE:"
				+ calculator.divisionOfTwoNumbers(Double.parseDouble(num1), Double.parseDouble(num2));
		}
	}
	
	@GetMapping("/salary")
	public String CalculateFinalSalaryDependingTypeEmployee(@RequestParam(value = "initialSalary") String initialSalary,
			@RequestParam(value = "employeeType") String employeeType) {
		return "INITIAL SALARY:" + initialSalary + "   EMPLOYEE TYPE:" + employeeType + "   FINAL SALARY:"
				+ salarycalculator.CalculateFinalSalaryDependingTypeEmployee
				(Double.parseDouble(initialSalary), employeeType);
	}
}


