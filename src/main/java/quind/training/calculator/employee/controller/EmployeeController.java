package quind.training.calculator.employee.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import quind.training.calculator.employee.data.IEmployeeDao;
import quind.training.calculator.employee.domain.Employee;

import java.util.Collection;

@RestController
public class EmployeeController {
	
	@Autowired
	private IEmployeeDao iemployeeDao;

	public EmployeeController() {}
	
	public EmployeeController(IEmployeeDao employeeDAO) {
		this.iemployeeDao = employeeDAO;
	}

	@GetMapping("/employee")
	public Collection<Employee> findAll() {
		return iemployeeDao.findAll();
	}
	
	@GetMapping("/employee/{id}")
	public Employee get(@PathVariable int id) {
		return iemployeeDao.findById(id);
	}
	
	@PostMapping("/employee")
	public String add(@RequestBody Employee employee) {
		return iemployeeDao.save(employee);
	}
	


	@PutMapping("/employee")
	public String update(@RequestBody Employee employee) {
		return iemployeeDao.update(employee);
	}
	
	
	@DeleteMapping("/employee/{id}")
	public String delete(@PathVariable int id) {
		return ""+iemployeeDao.delete(id);
	}
	
	@GetMapping("/salesup2")
	public String index() {
		return ("200");
	}
	
}
