package quind.training.calculator.employer.unittest;

import org.junit.Test;

import quind.training.calculator.employee.domain.Calculator;

import org.junit.Assert;

public class OperationTest {

	@Test
	public void testSumOfTwoDecimalNumbersTest() {
		// Arange
		double numSum1 = 4;
		double numSum2 = 2;
		double resultExpectedSum = 6;

		Calculator calculator = new Calculator(numSum1, numSum2);

		// Act
		double resultSum = calculator.sumOfTwoNumbers(numSum1, numSum2);

		// Assert
		Assert.assertEquals(resultExpectedSum, resultSum, 0);
	}

	@Test
	public void testSubtractOfTwoDecimalNumbersTest() {
		// Arange
		double num1Subtract1 = 4;
		double num1Subtract2 = 2;
		double resultExpectedSubtraction = 2;

		Calculator calculator = new Calculator(num1Subtract1, num1Subtract2);

		// Act
		double resultSubtract = calculator.subtractOfTwoNumbers(num1Subtract1, num1Subtract2);

		// Assert
		Assert.assertEquals(resultExpectedSubtraction, resultSubtract, 0);
	}

	@Test
	public void testMultiplicationOfTwoDecimalNumbersTest() {
		// Arange
		double numMultiplication1 = 4;
		double numMultiplication2 = 2;
		double resultExpectedMultiplication = 8;

		Calculator calculator = new Calculator(numMultiplication1, numMultiplication2);

		// Act
		double resultMultiplication = calculator.MultiplicationOfTwoNumbers(numMultiplication1, numMultiplication2);

		// Assert
		Assert.assertEquals(resultExpectedMultiplication, resultMultiplication, 0);
	}
 
	@Test
	public void testDivisionOfTwoDecimalNumbersTest() {
		// Arange
		double numDivision1 = 4;
		double numDivision2 = 2;
		double resultExpectedDivision = 2;
		double resultDivision = 0;

		Calculator calculator = new Calculator(numDivision1, numDivision2);

		// Act
		if (numDivision2 ==0) {System.out.println("ERROR: This operation cannot be realized");}
		else {
		resultDivision = calculator.divisionOfTwoNumbers(numDivision1, numDivision2);
		}
		// Assert
		Assert.assertEquals(resultExpectedDivision, resultDivision, 0);
	}

}
