FROM openjdk:8-jdk-alpine
EXPOSE 9090
WORKDIR /calculator-ultimate
VOLUME /tmp
COPY ./build/libs/*.jar ./app.jar
ENTRYPOINT ["java","-jar","/calculator-ultimate/app.jar"]